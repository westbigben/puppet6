node default {
  include roles::default
}

node /^cdhedge1*/, /^cdpedge*/, /^hdpedge*/ { 
  include roles::edge
}

node /^torch1/ {
  include roles::default
}
