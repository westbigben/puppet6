class adacct::devops {
  user { 'nitinb' :
    ensure	=> present,
    comment	=> 'Nitin Bharadwaj',
    expiry	=> absent,
    gid		=> 'users',
    groups	=> 'wheel',
  }
  file { '/home/nitinb' :
    ensure	=> directory,
    owner	=> 'nitinb',
    group	=> 'users',
    mode	=> '700',
  }
  ssh_authorized_key { 'nitinb@acceldata.io' :
    ensure 	=> present,
    user	=> 'nitinb',
    type	=> 'ssh-rsa',
    key		=> 'AAAAB3NzaC1yc2EAAAADAQABAAABgQD7FSRgx7eWy7JOSG99hVMk/4ndOnhGlff//ipHgiTTRmfFPAmAnqhV3coAsKj0ZVZWRLqLziqllYmQcKGEUh22KJd4Pg8kVAAnghxG1c/g6eOqIOIaXixCNIj8nWtylOX97SgJENFp8GLSWGL8UqKhzJCRY3v48AXAgbH3BD8k0B+3aZcjwxTfwzgDM5rAzQwk87fMEtxKrs+5U8+a1Wk9yLkG0obGzsxrv3+FUza7mGUhqOie3ziKZj6MN1XZ61OipIZ1I14RAoTv+gsKbd6s08tU3g13yqOi/qlKezf8cQfQNRMCL2ssHDIEiEVkTWPABl7Myc5mKj9U2LwokJq3FNl4xEl3UJ7u9aRDmAdZIRuUcgpi6l7OFbc8TpBCT/9dNJrmI7TobgBjAqnrCr0Yk9El1I+CC34hurg+KDgNLnbJpqRUk1aEJKhld/0eMrA1SrP0QFHwtwBU5Egw2mu4WFao0IRpC34umsp+um0p3TCcOXVK6IO4yMUAsbQp4As=',
    require	=> File['/home/nitinb'],
  }
}
