class adacct::groups {
  group { 'devops' :
    ensure	=> 'present',
    gid		=> '1500',
  }
  group { 'dev' :
    ensure	=> 'present',
    gid		=> '1600',
  }
  group { 'qe' :
    ensure	=> 'present',
    gid		=> '1700',
  }
  group { 'sre' :
    ensure	=> 'present',
    gid		=> '1800',
  }
}
