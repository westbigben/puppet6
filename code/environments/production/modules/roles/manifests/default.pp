class roles::default {
  include ::accounts
  include profiles::motd
  include profiles::ntp
  include profiles::resolv
  include profiles::sel
  include profiles::sshcfg
  include profiles::tools
}
