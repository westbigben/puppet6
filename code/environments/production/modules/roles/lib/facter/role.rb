Facter.add('role') do
    setcode do
    # Retrieve hostname and assign role base on it
    hname = Facter.value(:hostname)
      case hname
          when /^cdh[0-9]/
            'cdh'
          when /^cdhedge[0-9]/
            'cdhedge'
          when /^cdp[0-9]/
            'cdp'
          when /^cdpedge[0-9]/
            'cdpedge'
          when /^hdpedge[0-9]/
            'hdpedge'
          when /^hdp[0-9]/
            'hdp'
          when /^cmk[0-9]/
            'cmk'
        end
    end
end
