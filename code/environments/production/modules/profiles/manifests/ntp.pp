class profiles::ntp {
  $servers       = hiera('profiles::ntp::servers')
  $restrict      = hiera('profiles::ntp::restrict')
  $iburst_enable = hiera('profiles::ntp::iburst_enable', false)

  class { 'ntp':
    servers       => $servers,
    restrict      => $restrict,
    iburst_enable => $iburst_enable,
  }
}
