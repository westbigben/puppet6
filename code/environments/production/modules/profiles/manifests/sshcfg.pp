class profiles::sshcfg {
  class { 'ssh':
    storeconfigs_enabled => false,
    server_options => {
      'PasswordAuthentication' => 'yes',
      'ChallengeResponseAuthentication' => 'yes',
      'GSSAPIAuthentication'	=> 'yes',
      'GSSAPICleanupCredentials' => 'no',
      'UsePAM'	=> 'yes',
      'PrintMotd' => 'yes',
      'AllowTcpForwarding' => 'yes',
      'X11Forwarding' => 'no',
    },
    client_options => {
      'Host *'  => {
        'SendEnv'	=> 'LANG LC_*',
        'StrictHostKeyChecking' => 'no',
	'GSSAPIAuthentication' => 'yes',
      },
    },
  }
}
