# Class: profiles::tools::default
#
# Parameters: none

class profiles::tools {

  $no_nmap = hiera('profiles::no_nmap',true)
  $imp_tools = ['strace','bind-utils','wget','curl','gcc','tmux','htop','ipa-client','lsof','clang','clang-devel','make','gdb','yum-versionlock','iptraf','iperf','iperf2','ipcalc','wireshark','iotop','rsync','tcpdump']

  package { $imp_tools :
    ensure => latest,
  }
 
  if !defined(Package['tree']) {
    package { 'tree': ensure => present }
  }

  if !defined(Package['dos2unix']) {
    package { 'dos2unix': ensure => present }
  }

  if !defined(Package['iotop']) {
    package { 'iotop': ensure => present }
  }

  if !defined(Package['sysstat']) {
    package { 'sysstat': ensure => latest }
  }

  if !defined(Package['vim-enhanced']) {
    package { 'vim-enhanced': ensure => present }
  }
  
  if $no_nmap {
    case $::operatingsystemmajrelease {
      '7':     {
        if !defined(Package['nmap-ncat']) {
          package { 'nmap-ncat': ensure => absent }
        }
      }
      default: {
        if !defined(Package['nc']) {
          package { 'nc': ensure => absent }
        }
      }
    }
  }

  if !defined(Package['telnet']) {
    package { 'telnet': ensure => absent }
  }

  if !defined(Package['mailx']) {
    if $facts['role'] != 'mta' {
      package { 'mailx': ensure => present }
    }
  }

  if ! defined(Package['authconfig']) {
    package { 'authconfig' : ensure => present }
  }

 if !defined(Package['screen']) {
    package { 'screen': ensure => present }
  }

  if !defined(Package['unzip']) {
    package { 'unzip': ensure => present }
  }

  if !defined(Package['zip']) {
    package { 'zip': ensure => present }
  }

  if !defined(Package['at']) {
    package { 'at': ensure => present }
  }

  if !defined(Package['bc']) {
    package { 'bc': ensure => present }
  }

  if !defined(Package['lftp']) {
    package { 'lftp': ensure => present }
  }

  package { 'bash': ensure => latest }

  # sudo vuln
  package { 'sudo' : ensure => latest }

  # installing git
  package { 'git' : ensure => latest }

  # including jq
  package { 'jq' : ensure => latest }
 
  # include growpart
  package { 'cloud-utils-growpart-0.29-5.el7.noarch' : ensure => latest }
  
}
