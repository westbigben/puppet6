class profiles::resolv {

  $nameservers = hiera('profiles::resolv::nameservers')
  $domain = hiera('profiles::resolv::domain')
  $searchpath = hiera('profiles::resolv::searchpath')

  file { '/etc/resolv.conf':
    ensure	=> file,
    owner	=> 'root',
    group	=> 'root',
    mode	=> '0644',
    content	=> template('profiles/resolv.conf.erb'),
  }

}
