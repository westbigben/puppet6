class profiles {
  include profiles::resolv
  include profiles::motd
  include profiles::sel
  include profiles::sshconfig
  include profiles::tools
  include profiles::docker
}
