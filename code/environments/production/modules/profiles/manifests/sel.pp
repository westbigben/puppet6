class profiles::sel {
  if ($facts['os']['family'] != 'Debian') {
    class { 'selinux': 
      mode => 'permissive',
    }
  }
}
