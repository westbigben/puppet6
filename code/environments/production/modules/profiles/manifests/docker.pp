class profiles::docker {
  
# Class: profiles::docker
#
# Sample Usage:
#
# include profiles::docker
#
# Author: nitinb

  $docker_ce_packages = ['docker-ce','docker-ce-cli','containerd.io','lvm2','device-mapper-persistent-data']

  package { $docker_ce_packages :
    ensure => installed,
    require => File['/etc/yum.repos.d/docker-ce.repo'],
  }

  file { '/etc/yum.repos.d/docker-ce.repo' :
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => 'puppet:///modules/profiles/docker/docker-ce-centos-7.repo',
  }

  file { '/etc/docker':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  service { 'docker' :
    enable => true,
    ensure  => 'running',
    require => Package['docker-ce'],
  }

}
