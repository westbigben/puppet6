class profiles::motd {
  file {'/etc/motd':
  content  => '
    _                _     _       _            ___
   / \   ___ ___ ___| | __| | __ _| |_ __ _    |_ _|___    ___
  / _ \ / __/ __/ _ \ |/ _` |/ _` | __/ _` |    | ||  _ \ / __|
 / ___ \ (_| (_|  __/ | (_| | (_| | || (_| |_   | || | | | (__ _
/_/   \_\___\___\___|_|\__,_|\__,_|\__\__,_( ) |___|_| |_|\___(_)
                                           |/

UNAUTHORIZED ACCESS  PROHIBITED! Only Authorized Access Permitted
Disconnect IMMEDIATELY if you are not an authorized user.

NOTICE:
Unauthorized access, use, misuse, or modification of this computer system
or of the data contained herein or in transit to/from this system is prohibited!!

This system and equipment are subject to monitoring to ensure proper performance
of applicable security features or procedures. Such monitoring may result
in the acquisition, recording and analysis of all data being communicated,
transmitted, processed or stored in this system by a user.

Please disconnect IMMEDIATELY if you do not agree to these conditions.

',
}

}
